/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Models;
import Events.LoginListener;
import Controllers.LoginController;
import Views.Auth;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import javax.swing.JOptionPane;

/**
 *
 * @author Sieg
 */
public class Login {
    private String username;
    private String password;
    private LoginListener loginListener;
    private static Statement statement;
    private static ResultSet result;
    
    public Login(){
        Config.RunConnection();
        statement = Config.statement;
        result = Config.result;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
        fireOnChange();
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
        fireOnChange();
    }

    public LoginListener getLoginListener() {
        return loginListener;
    }

    public void setLoginListener(LoginListener loginListener) {
        this.loginListener = loginListener;
        fireOnChange();
    }
    
     public void fireOnChange(){
        if(loginListener != null)
        {
            loginListener.onChange(this);
        }
    }
     
    public void confirm() throws SQLException{
        try{
            String Data = "SELECT * from users WHERE user_name =    ";
            
            
        } catch (Exception e){
            System.out.println(e.getMessage());
        }
    }
}
