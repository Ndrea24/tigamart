/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Models;

import Events.UsersListener;
import static Models.Config.result;
import static Models.Config.statement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import javax.swing.JOptionPane;
import java.sql.Connection;
import java.sql.SQLException;
import javax.swing.table.DefaultTableModel;
/**
 *
 * @author FAIZAL
 */
public class Users {

    //Columns
    private String user_id;
    private String user_name;
    private String user_password;
    private String user_role;
    private String user_address;
    
    //Listener
    private UsersListener usersListener;

    DefaultTableModel model = new DefaultTableModel();
    
    public Users(){
        model.addColumn("User ID");
        model.addColumn("Nama Kasir");
        model.addColumn("Alamat");
    }
    
    public UsersListener getUsersListener(){
        return usersListener;
    }
   
    public void setUsersListener(UsersListener usersListener){
        this.usersListener = usersListener;
    }
    
      //_______________________________________________________________________//
     //     FIRE ON CHANGE                                                    //
    //_______________________________________________________________________//
    protected void fireOnChange(){
        if(usersListener != null)
        {
            usersListener.onChange(this);
        }
    }
    
      //_______________________________________________________________________//
     //   GET VALUES FROM COLUMNS                                             //
    //_______________________________________________________________________//

    public String getUserId(){
        return user_id;
    }

    public String getUserName(){
        return user_name;
    }

    public String getUserPassword(){
        return user_password;
    }    
        
    public String getUserRole(){
        return user_role;
    }
    
    public String getUserAddress(){
        return user_address;
    }
      //_______________________________________________________________________//
     //   SET VALUES FROM COLUMNS                                             //
    //_______________________________________________________________________//
    public void setUserId(String user_id){
        this.user_id = user_id;
        fireOnChange();
    }
    
    public void setUserPassword(String user_password){
        this.user_password = user_password;
        fireOnChange();
    }
    
    public void setUserName(String user_name){
        this.user_name = user_name;
        fireOnChange();
    }
    
    public void setUserRole(String user_role){
        this.user_role = user_role;
        fireOnChange();
    }
    
    public void setUserAddress(String user_address){
        this.user_address = user_address;
        fireOnChange();
    }
    
      //_______________________________________________________________________//
     //   FORM FUNCTION                                                            //
    //______________________________________________________________________//
    public void resetForm(){
        setUserName("");
        setUserPassword("");
        setUserRole("");
        setUserAddress("");
    }
    
    public void saveData() throws SQLException{
        System.out.println(getUserId());
        System.out.println(getUserName());
        System.out.println(getUserPassword());
        System.out.println(getUserRole());
        System.out.println(getUserAddress());
        try{
            String Query = "INSERT INTO users (user_id , user_name, user_password, user_role, user_address) VALUES ('%s','%s','%s','%s','%s')";
            Query = String.format(Query, this.getUserId(), this.getUserName(), this.getUserPassword(), this.getUserRole(), this.getUserAddress());
             if(!statement.execute(Query)){
                JOptionPane.showMessageDialog(null, "Kasir atas nama : '"+this.getUserName()+"' telah terdaftar, dengan User ID : '"+this.getUserId()+"'");
                resetForm();
             }else{
                JOptionPane.showMessageDialog(null, "Gagal Disimpan");
            }
        }catch(Exception e){
            System.out.println(e);
        }
    }
    
    public void updateData() throws SQLException {
        String Query = "UPDATE users set user_name = '%s', user_role = '%s', user_address='%s' ";
    }
    
    public DefaultTableModel loadTable() throws SQLException{
         String Query = "SELECT * FROM users ORDER BY user_name";
         java.sql.Connection conn = Config.configDB();
         java.sql.PreparedStatement pst = conn.prepareStatement(Query);
         java.sql.ResultSet rs = pst.executeQuery();
         model.setNumRows(0);
         while(rs.next()){
            model.addRow(new Object[]{rs.getString(1), rs.getString(2), rs.getString(5)});
         }
         return model;
    }
   

}