/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Events;
import Models.Transaksi;
/**
 *
 * @author Sieg
 */
public interface TransaksiListener {
    public void onChange(Transaksi transaksi);
}
