/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controllers;

import Models.Users;
import Views.UsersIndex;
import Views.UsersIndex;
import javax.swing.JOptionPane;
import java.sql.SQLException;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.sql.SQLException;
import javax.swing.JOptionPane;
import Models.Config;
import Models.Users;

/**
 *
 * @author FAIZAL
 */
public class UsersController {
    private Users model;
    public void setModel(Users model){
        this.model = model;
    }
    
    public void resetForm(UsersIndex view){
        String Uname = view.getUname().getText();
        String Upass = view.getUpass().getPassword().toString();
        String Uaddress = view.getUaddress().getText();
        
        if(Uname.equals("") && Upass.equals("") && Uaddress.equals("")){
            JOptionPane.showMessageDialog(view, "KOSONG!");
        }
        else{
            model.resetForm();
        }
        
    }
    
    public void saveForm(UsersIndex view) throws SQLException{
        String user_name = view.getUname().getText();
        String user_password = view.getUpass().getPassword().toString();
        String user_address = view.getUaddress().getText();
        String user_role = "Cashier";
        String user_id = this.generateUserId();
        model.setUserId(user_id);
        model.setUserName(user_name.trim());
        model.setUserPassword(user_password);
        model.setUserRole(user_role);
        model.setUserAddress(user_address.trim());
        model.saveData();
    }
    
    protected String generateUserId(){
        String aChars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
        StringBuilder ast = new StringBuilder();
        Random ran = new Random();
        while(ast.length() < 6){
            int index = (int) (ran.nextFloat() * aChars.length());
            ast.append(aChars.charAt(index));
        }
        String astr = ast.toString();
        return astr;
    }
    
    public static void main(String args[]) {
       UsersIndex pv;
        try {
            pv = new UsersIndex();
            pv.setVisible(true);
        } catch (SQLException ex) {
            Logger.getLogger(UsersController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
  
}
